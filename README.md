# TS HAM Link v.0.3.5

Pluginnet er kodet i både C og C++, til både Windows og Linux. Denne her file beskriver de seneste ændringer og rettelser som er fortaget i dette projekt. 

## Formål og funktion med dette plugin

Dette TS3 plugin gør det muligt at tilkoble en radio-repeater via serial port via en Arduino Nano. Denne sketch ligger i mappen "arduino" i roden af projektet.

## Rettelser for denne version
Lavet nu definetion: RXROGERBEEPPREDELAY til indstilling af deklay mellem en handling afsluttes og tone aktivers vis angivet.

## Ny funktioner for denne version

 - • Lavet code-cleanup i TSHAMLink plugin TS3.

## Kompatibilitet

| Styresystem |  TeamSpeak version | Plugin SDK Version 
| :---: | :---: | :---: |
| Windows 10/11 x64 | v.3.6.2 | v.26
| Ubuntu 22:04 x64 | v.3.6.2 | v.26


## Kompilere plugin for Windows 10/11 x64

Du skal have installeret "Make" og "MSYS2" med GCC kompiler for at kunne kompilere projektet til en TS3 Windows x64 klient.

Vis du allerede har installeret disse kan du bare eksikvere denne kommando i roden af projektet:

    make -f .\Plugin-Windows.mk

Dette ville kompilere en ".dll" fil i mappen "/bin" som ligger i roden af projektet.
Denne ".dll" er selve pluginnet som ligges i TS3 klientes plugins mappe.

Vis ikke du har installeret MSYS2 GCC og Make, skal du installere dem som vist her efter, og køre make komandoen igen for bygge pluginnet til Windows.

### Installering af MSYS2 (GCC)

Gå ind på følgende link https://www.msys2.org/ og følge instruktionerne for installere GCC  

### Installering af Make

Du skal først installere Chocolatey ved at følge instruktionerne på dette link: https://chocolatey.org/install.

Her efter åbner du powershell og taster følgende kommando: 

    choco install make

Dette installere Make til Windows via Chocolatey package-manger.

## Kompilere plugin for Linux (Ubuntu 22:04) x64

Du skal have installere "Make" og "GCC" for kunne kunne kompilere projektet i Ubuntu.

Vis allrede du har installeret disse, kan du bare køre følgkede komanodo vi bash-prompten i roden af mappen:

    make -f Plugin-Linux.mk

Dette ville kompilere en ".so" fil i mappen "/bin" som ligger i roden af projektet.
Denne ".so" er selve pluginnet som ligges i TS3 klientes plugins mappe.

Vis ikke du har installeret Make og GCC skal du installere dette først og køre denne her komando efterfølgede igen.

### Installering af Make og GCC

Kør følgende komando for at installer "Make" og "GCC" på din Linux (Ubuntu) maskine:

    sudo apt update
    sudo apt install make gcc

Dette installere Make og GCC via Ubuntu's inbyggede package-manger.

## Build via docker container
Du kan vælge at opsætte det medfølgede docker image build build af pluginnet.
> 💡 Der er på nuværede tidspunkt ikke lavet et docker image til build for TSHAMLink til Windows x64

### Build docker images
For at bygge image for build af TSHAMLink (Linux) i eget lokal docker repo:
```
docker build -f .\docker\LinuxBuildDockerfile -t danielswebsite/tshamlink:linux-build .
```

### Run docker build container
For at starte en midlertidig container til linux build:
```
docker run --rm --name TSHAMLinkLinuxBuild -it danielswebsite/tshamlink:build-linux
```

### Mounts and volums
|Volume (Linux)           |Description                |
|-------------------------|---------------------------|
|/project/TS-HAM-Link/bin |Output for the pluginfile  |
|/project/TS-HAM-Link/logs|Logs from the build process|


### Enveriment variabler og argumenter
Brug 'BRANCH/branch' som env eller argument til sætte den branch du ønsker at bygge TSHANLink ud fra.
Se i dette git repo hvilke versioner/branch du kan bygge.

ENV Eks:
```
docker run --rm -e BRANCH=version/0.3.4 --name TSHAMLinkLinuxBuild -it danielswebsite/tshamlink:build-linux
```

Argument eks:
```
docker run --rm --name TSHAMLinkLinuxBuild -it danielswebsite/tshamlink:build-linux -branch version/0.3.4
```