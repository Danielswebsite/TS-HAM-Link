#
# Makefile to build TS HAM Link Plugin for TeamSpeak 3 x64 for Linux
#

GCC = g++
CFLAGS = -c -O2 -Wall -fPIC
PNAME = tshamlink_0_3_5

all: plugin clean

plugin: plugin.o Repeater.o RepeaterWrapper.o PluginConfig.o SerialPort.o Helper.o
	$(GCC) -o bin/$(PNAME).so -shared obj/plugin.o obj/SerialPort.o obj/Helper.o obj/Repeater.o obj/RepeaterWrapper.o obj/PluginConfig.o

plugin.o: ./src/plugin.c
	$(GCC) $(CFLAGS) -Iinclude src/plugin.c -o obj/plugin.o

Repeater.o: ./src/Repeater.cpp
	$(GCC) $(CFLAGS) -Isrc src/Repeater.cpp -o obj/Repeater.o

RepeaterWrapper.o: ./src/RepeaterWrapper.cpp
	$(GCC) $(CFLAGS) -Isrc src/RepeaterWrapper.cpp -o obj/RepeaterWrapper.o

PluginConfig.o: ./src/PluginConfig.c
	$(GCC) $(CFLAGS) -Isrc src/PluginConfig.c -o obj/PluginConfig.o

SerialPort.o: ./src/SerialPort.cpp
	$(GCC) $(CFLAGS) -Isrc src/SerialPort.cpp -o obj/SerialPort.o

Helper.o: ./src/Helper.cpp
	$(GCC) $(CFLAGS) -Isrc src/Helper.cpp -o obj/Helper.o

clean:
	rm -rf obj/*.o
