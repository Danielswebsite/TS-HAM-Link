// TSHAMLink Arduino skecth for controlling of radio/repeater serial connection
// This arduino sketch is for TSHAMLink version 0.3.4

// Defines states.
#define RESIVESIGNALSTART "HRF01"
#define RESIVESIGNALEND "HRF02"
#define SENDINGSIGNALSTART "HRT01"
#define SENDINGSIGNALEND "HRT02"
#define STATEERROR "ERR01"
#define HANDSHAKECHECK "HST01"
#define HANDSHAKEACCEPTED "HST02"
#define WATINGFORHANDSAKE "WSC01"
#define DISCONNECT "DIS01"
#define DISCONNECTED "DIS02"

// Defines TX/RX pins for radio/repeater
#define TXPIN 8 // 5V out to radio/repeater for PTT
#define RXPIN 7 // 5V in from radio/repeater for resiving

// Roger-beep settings
#define ROGERBEEPPEN 9 // Digital sound out for roger-beep 
#define TXROGERBEEPENABLE true // Enable (true) or disable (false) roger-beep
#define RXROGERBEEPENABLE true // Enable (true) or disable (false) roger-beep
#define RXROGERBEEPPREDELAY 700 // Delay before tone will start aftner signal from radio to TS is done (>=0)

// Signal ending settings
#define TXSIGNALENDDELAY 100 // Set delay afther sender is get command to stop output 5v on TX-pin on TX side
#define RXSIGNALENDDELAY 100 // Set delay afther sender is get command to stop output 5v on TX-pin on RX side


// Command state
String c_state = WATINGFORHANDSAKE;

// Setup.
void setup()
{
	Serial.begin(57600);
	Serial.setTimeout(100);
	enablePins();
}

// Loop.
void loop()
{
	pluginHandShake();
	if (!stateMatch(RESIVESIGNALSTART))
		sendSignal();

	if (!stateMatch(SENDINGSIGNALSTART))
		resiveSignal();
}

// Make handshape with TSHAMLink plugin
void pluginHandShake()
{	
	while (stateMatch(WATINGFORHANDSAKE))
	{
		if (Serial.available() <= 0)
			continue;

		if (!setState(Serial.readString()))
		{
			Serial.print(STATEERROR);
			setState(WATINGFORHANDSAKE);
			continue;
		}

		else if (stateMatch(HANDSHAKECHECK))
		{
			Serial.print(HANDSHAKEACCEPTED);
			break;
		}

		else
		{
			Serial.print(STATEERROR);			
		}
	}
}

// Enable dig. pins
void enablePins()
{
	pinMode(TXPIN, OUTPUT);
	pinMode(RXPIN, INPUT);
}

// Set the new state.
bool setState(String state)
{
	if (stateMatch(state))
		return false;

	c_state = state;
	return true;
}

// Set the state in disconnec mode and reset all pinns an setttings for a new connection
void disconnect() {
	if(!stateMatch(DISCONNECT))
		return;
	
	digitalWrite(TXPIN, LOW);

	if (!setState(WATINGFORHANDSAKE))
		return;
	
	Serial.print(DISCONNECTED);
}

// When the radio get 5v from this Arduino board.
void sendSignal()
{
	if (Serial.available() <= 0)
		return;

	if (!setState(Serial.readString()))
	{
		Serial.print(STATEERROR);
		return;
	}

	if (String(SENDINGSIGNALSTART) == c_state)
		digitalWrite(TXPIN, HIGH);

	else if (String(SENDINGSIGNALEND) == c_state)
	{
		if(TXROGERBEEPENABLE) {
			beep(1000, 255, 100);
		}
		delay(TXSIGNALENDDELAY);
		digitalWrite(TXPIN, LOW);
	}

	else if(String(DISCONNECT) == c_state)
		disconnect();
}

// Check for is a state match in statemachine
bool stateMatch(String state)
{
	return c_state == state;
}

// When the radio sending 5v to this Arduino board.
void resiveSignal()
{
	bool r_input = (bool)digitalRead(RXPIN);
	if (!r_input && !stateMatch(RESIVESIGNALSTART))
		return;

	else if (r_input && setState(RESIVESIGNALSTART))
		Serial.print(c_state);
	
	if(!r_input && setState(RESIVESIGNALEND)) 
	{
		digitalWrite(TXPIN, HIGH);
		delay(RXROGERBEEPPREDELAY);		
		if(RXROGERBEEPENABLE) {
			beep(1000, 255, 100);
		}	
		delay(RXSIGNALENDDELAY);
		digitalWrite(TXPIN, LOW);	
		Serial.print(c_state);
	}
}

// Beep tone
// frq=Frequency of beep-tone
// amp=Ampery/power of the Digital output 255=max-power
// ms=Tone-length in microsecunds
void beep(unsigned int frq, int amp, int ms)
{
    for (size_t i = 0; i < int(frq * (double(ms) / double(1000))); i++)
    {
        analogWrite(ROGERBEEPPEN, amp);
        delayMicroseconds((1000000 / frq) / 2);
        analogWrite(ROGERBEEPPEN, 0);
        delayMicroseconds((1000000 / frq) / 2);
    }
}


