#ifdef __cplusplus
extern "C" {
#endif

	typedef struct Repeater Repeater;	
	typedef unsigned char boolean;
	
	static const boolean False = 0;
	static const boolean True = 1;
	
	Repeater* newRepeater();	
	void Repeater_tryconnect(Repeater* r, boolean* success);
	void Repeater_disconnect(Repeater* r, boolean* success);
	void Repeater_radioSend(Repeater* r);
	void Repeater_radioStopSend(Repeater* r);
	void Repeater_onResiving(void(*f)(boolean), Repeater* r);

#ifdef __cplusplus
}
#endif





