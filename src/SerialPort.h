#include <list>
#include <string>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <cstdio>
#include <iostream>
#include <filesystem>
#ifdef _WIN32
#include <Windows.h>
#include <tchar.h>
#else
#include <fcntl.h> // Contains file controls like O_RDWR
#include <errno.h> // Error integer and strerror() function
#include <termios.h> // Contains POSIX terminal control definitions
#include <unistd.h> // write(), read(), close()
#include <dirent.h>
#endif

using namespace std;

namespace dch {
	class SerialPort
	{
	public:
#ifdef _WIN32
		static list<string> getComportNames();
		void connect(string c_name, int b_rate, BYTE dtr_enable, DWORD rts_enable, BYTE parity, BYTE bit_size);
		string read();
		void write(string c_text);
		void close();
#else
		static list<string> getTTYNames();
		int connectPort(char* c_name);
		int readMsg(char* buffer, int size);
		int writeMsg(const char* buffer, int size);
		void closePort();
#endif
	private:
#ifdef _WIN32
		HANDLE serialhandler;
#else		
		int serial_port;
#endif
	};
};



