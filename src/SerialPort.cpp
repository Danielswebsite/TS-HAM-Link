#include "SerialPort.h"
#ifdef _WIN32
void dch::SerialPort::connect(string c_name, int b_rate, BYTE dtr_enable, DWORD rts_enable, BYTE parity, BYTE bit_size) {
	wstring sw_name = wstring(c_name.begin(), c_name.end());
	serialhandler = CreateFile(sw_name.c_str(), GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);

	if (serialhandler == INVALID_HANDLE_VALUE)
		throw("Exception: Could not open serialport");

	COMMTIMEOUTS cto = { MAXDWORD, 0 ,0 ,0, 0 };
	cto.ReadIntervalTimeout = 10;
	cto.WriteTotalTimeoutConstant = 100;
	cto.ReadTotalTimeoutConstant = 100;
	DCB dcb;

	if (!SetCommTimeouts(serialhandler, &cto))
		throw("Exception; Could not set timeouts on com-port");

	memset(&dcb, 0, sizeof(dcb));
	dcb.DCBlength = sizeof(dcb);
	dcb.BaudRate = b_rate;
	dcb.fBinary = 1;
	dcb.fDtrControl = dtr_enable; //DTR_CONTROL_ENABLE; 
	dcb.fRtsControl = rts_enable;  //RTS_CONTROL_ENABLE;
	dcb.Parity = parity; //ODDPARITY; // Only work with Leonardo	
	dcb.StopBits = ONE5STOPBITS;
	dcb.ByteSize = bit_size; // 8

	if (!SetCommState(serialhandler, &dcb))
		throw("Exception: Could not set comstate on com-port");
}

string dch::SerialPort::read()
{
	DWORD n_read;
	char c_buffer[1024];
	bool r_success = ReadFile(serialhandler, c_buffer, sizeof(c_buffer), &n_read, NULL);

	if (!r_success)
		return "";

	c_buffer[n_read] = '\0';
	return string(c_buffer);
}

void dch::SerialPort::write(string c_text) {

	int n = c_text.length();
	const char* c_array = c_text.c_str();

	DWORD n_write;
	WriteFile(serialhandler, c_array, strlen(c_array), &n_write, NULL);
}

void dch::SerialPort::close() {
	CloseHandle(serialhandler);
}

list<string> dch::SerialPort::getComportNames() {

	list<string> c_names;
	wchar_t buffer[5000];
	string c_name;
	bool has_port = false;

	DWORD c_test;
	wstring wc_name;
	for (int i = 0; i < 255; i++)
	{
		wc_name = L"COM" + to_wstring(i);
		c_test = QueryDosDevice(wc_name.c_str(), buffer, 5000);
		if (c_test != 0)
		{
			c_names.push_back(string(wc_name.begin(), wc_name.end()));
		}

		if (::GetLastError() == ERROR_INSUFFICIENT_BUFFER)
		{
			buffer[1000];
			continue;
		}
	}
	return c_names;
}
#else

int dch::SerialPort::connectPort(char* c_name) {
	serial_port = open(c_name, O_RDWR);

	struct termios tty;

	if (tcgetattr(serial_port, &tty) != 0)
	{
		printf("Error %i from tcgetattr: %s\n", errno, strerror(errno));
		return errno;
	}

	tty.c_cflag &= ~PARENB;
	tty.c_cflag &= ~CSTOPB;
	tty.c_cflag &= ~CSIZE;
	tty.c_cflag |= CS8;
	tty.c_cflag |= CREAD | CLOCAL;

	tty.c_lflag &= ~ICANON;
	tty.c_lflag &= ~ECHO;
	tty.c_lflag &= ~ECHONL;
	tty.c_lflag &= ~ISIG;
	tty.c_iflag &= ~(IXON | IXOFF | IXANY);
	tty.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL);

	tty.c_oflag &= ~OPOST;
	tty.c_oflag &= ~ONLCR;

	tty.c_cc[VTIME] = 100;
	tty.c_cc[VMIN] = 0;

	cfsetispeed(&tty, B57600);
	cfsetospeed(&tty, B57600);

	if (tcsetattr(serial_port, TCSANOW, &tty) != 0)
	{
		printf("Error %i from tcsetattr: %s\n", errno, strerror(errno));
		return errno;
	}

	return 0;
}

int dch::SerialPort::readMsg(char* buffer, int size) {
	return read(serial_port, buffer, size);
}

list<string> dch::SerialPort::getTTYNames()
{
	list<string> c_names;
	char* s_name = "/dev";
	dirent* path;
	char name[256];
	DIR* sdir;
	string c_name;

	if ((sdir = opendir(s_name)))
	{
		while ((path = readdir(sdir)) != NULL)
		{
			c_name = s_name;
			c_name.append("/");
			c_name.append(path->d_name);

			if (c_name.find("ttyUSB") != string::npos)
			{
				c_names.push_back(c_name);
			}
		}
		closedir(sdir);
	}

	return c_names;
}

int dch::SerialPort::writeMsg(const char* buffer, int size)
{
	return write(serial_port, buffer, size);
}

void dch::SerialPort::closePort() {
	close(serial_port);
}
#endif




