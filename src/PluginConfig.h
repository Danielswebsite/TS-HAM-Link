#define DATASIZE 1024

/* Settings variables */
extern unsigned int autoConnect;
extern char f_data[DATASIZE];

/* Handle  */
void loadConfig();
void saveConfig();
void setConfigStr(char* data);
void getConfigStr(char* conf);
void getPath();