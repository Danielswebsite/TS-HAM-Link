#include <stdio.h>
#include <string>
#ifdef _WIN32
#include <Windows.h>
#endif

using namespace std;

namespace dch {
	class Helper
	{
	public:
		static bool str_equals(char* str_1, char* str_2, unsigned short s_scope, unsigned short e_scope);							
	};
}