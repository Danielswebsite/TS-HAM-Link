#include "SerialPort.h"

//Defines states.
#define RESIVESIGNALSTART "HRF01"
#define RESIVESIGNALEND "HRF02"
#define SENDINGSIGNALSTART "HRT01"
#define SENDINGSIGNALEND "HRT02"
#define STATEERROR "ERR01"
#define HANDSHAKECHECK "HST01"
#define HANDSHAKEACCEPTED "HST02"
#define WATINGFORHANDSAKE "WSC01"
#define DISCONNECT "DIS01"
#define DISCONNECTED "DIS02"
#define CONNECTED "CON01"

namespace dch {
	class Repeater
	{
		private:
			string state;
			SerialPort serial;
			bool r_connected = false;			
		public:
			void tryconnect(bool* success);
			void disconnect(bool* success);
			void radioSend();
			void radioStopSend();
			void onResiving(void(*f)(bool));
	};
};

