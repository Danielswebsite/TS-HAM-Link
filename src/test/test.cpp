#include <stdio.h>
#include <iostream>
#include <string>
#include <vector>
#include <ctime>
#include <chrono>
#include "../PluginConfig.h"

#ifdef C_CODE_TEST
#include "../RepeaterWrapper.h"
#include <unistd.h>
#else
#include "../Repeater.h"
#endif

using namespace std;
using namespace std::chrono;
using namespace chrono_literals;
#ifndef C_CODE_TEST
using namespace dch;
#endif

#define TESTSUCCESS  true;
#define TESTFAIL false;

struct test_resutlt
{
    string msg;
    bool r;
};

vector<test_resutlt(*)(void)> functions;

void printResult(string* text, bool* is_succes) 
{            
    string p_text; 
    if (*is_succes)
    {
        p_text.append("[OK] ");           
    }
    else {
        p_text.append("ERROR] ");            
    }
    p_text.append(*text);  
    p_text.append("\n");      
    printf(p_text.c_str());      
}

void runTests() 
{
    test_resutlt result;    
    for(test_resutlt(*func)(void) : functions) 
    {           
        result = func();        
        printResult(&result.msg, &result.r);        
    }    
}

test_resutlt test_config() 
{
    test_resutlt result;
    result.msg = "PluginConfig test";    
    result.r = True;    
    printf("%s%d\n", "AutoConnect: ", autoConnect);       
    loadConfig();
    printf("%s%d\n", "AutoConnect: ", autoConnect);       
    autoConnect = 1; 
    printf("%s%d\n", "AutoConnect: ", autoConnect);     
    saveConfig();            
    return result;
}

test_resutlt test_serialport() 
{
    test_resutlt result;
    result.msg = "SerialPort test";
    result.r = true;

    #ifndef C_CODE_TEST
    SerialPort serial;    
    for(string p_name : serial.getTTYNames()) 
    {
        cout << "Name: " << p_name << "\n";
    }

    result.r = true;
    #endif

    return result;
}


#ifdef C_CODE_TEST
void on_resiveing(boolean is_talk) {

}
#else
void on_resiveing(bool is_talk) {

}
#endif

test_resutlt test_repeater() 
{
    test_resutlt result;
    result.msg = "Repester tryconnect test";
    result.r = false;

#ifdef C_CODE_TEST
    
    Repeater* c_rep = newRepeater();
    boolean c_bool = False;
    Repeater_tryconnect(c_rep, &c_bool);

    result.r = c_bool;

    if (!result.r)
        return result;

    Repeater_onResiving(on_resiveing, c_rep);
    printf("Resving event lister is started. Wating now 10 sec before disconnet\n");

    sleep(10);    

    Repeater_disconnect(c_rep, &c_bool);

    result.r = c_bool;
#else
    Repeater rep;
    rep.tryconnect(&result.r);

    if (!result.r)
        return result;

    rep.onResiving(on_resiveing);
    printf("Resving event lister is started. Wating now 10 sec before disconnet\n");

    sleep(10);

    rep.disconnect(&result.r);
#endif

    result.r = !result.r;
    return result;
}

int main() 
{            
    functions.push_back(test_repeater);   
    functions.push_back(test_serialport);
    functions.push_back(test_config);
    runTests(); 
    return 0;
}

