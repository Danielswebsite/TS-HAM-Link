#include "Repeater.h"
#include "Helper.h"
#include <thread>
#include <future>

#ifdef _WIN32
#include <excpt.h>
#endif

using namespace dch;

void dch::Repeater::tryconnect(bool* success)
{
#ifdef _WIN32
	//TODO Check for om catch lukker comporten rigtig p� windows 32
	for (auto const& c_port : SerialPort::getComportNames())
	{
		try
		{
			serial.connect(c_port, 57600, DTR_CONTROL_ENABLE, RTS_CONTROL_DISABLE, PARITY_NONE, 8);
			Sleep(2000);

			serial.write(HANDSHAKECHECK);
			Sleep(2000);

			string t_str = serial.read();

			cout << "Port: " + c_port + " Out: " << t_str << "\n\r";

			if (t_str == HANDSHAKEACCEPTED)
			{
				*success = true;
				state = CONNECTED;
				break;
			}

			serial.close();
		}
		catch (const char* error)
		{
			cout << "Could not connect to " << c_port << "\n\r";
			*success = false;
		}
	}
#else

	string str_msg;
	char read_buffer[5];
	bool is_connected = false;
	for (auto const& c_port : SerialPort::getTTYNames())
	{
		try
		{
			if (serial.connectPort((char*)c_port.c_str()))
				throw "Could not connect to serial port";

			is_connected = true;

			sleep(2);

			str_msg = HANDSHAKECHECK;

			if (0 >= serial.writeMsg(str_msg.c_str(), str_msg.size()))
				throw "No data writen to serial-port";

			sleep(2);

			serial.readMsg(read_buffer, sizeof(read_buffer));
			string s_msg();

			if (Helper::str_equals(read_buffer, HANDSHAKEACCEPTED, 0, 5))
			{
				*success = true;
				state = CONNECTED;
				break;
			}

			serial.closePort();
			is_connected = false;
		}
		catch (const char* error)
		{
			if (is_connected)
			{
				serial.closePort();
				is_connected = false;
			}

			cout << error;
			*success = false;
		}
	}
#endif
}

void dch::Repeater::radioSend() {
#ifdef _WIN32
	serial.write(SENDINGSIGNALSTART);
#else
	string msg = SENDINGSIGNALSTART;
	serial.writeMsg(msg.c_str(), msg.size());
#endif
}

void dch::Repeater::radioStopSend() {

#ifdef _WIN32
	serial.write(SENDINGSIGNALEND);
#else
	string msg = SENDINGSIGNALEND;
	serial.writeMsg(msg.c_str(), msg.size());
#endif
}

void dch::Repeater::onResiving(void (*f)(bool)) {
	auto f_listner = [](string* state, SerialPort* serial, void (*f)(bool)) {

#ifndef _WIN32
		char buffer[512];
#endif 

		while (true)
		{
#ifdef _WIN32
			* state = serial->read();

			if (*state == DISCONNECTED)
				break;

			if (!(*state == RESIVESIGNALSTART ^ *state == RESIVESIGNALEND))
				continue;

			f(*state == RESIVESIGNALSTART);
			Sleep(200);
#else			
			if (serial->readMsg(buffer, sizeof(buffer)) <= 0)
				continue;

			*state = string(buffer);

			if (Helper::str_equals(buffer, DISCONNECTED, 0, 5))
				break;

			if (!(Helper::str_equals(buffer, RESIVESIGNALSTART, 0, 5) ^ Helper::str_equals(buffer, RESIVESIGNALEND, 0, 5)))
				continue;

			f(Helper::str_equals(buffer, RESIVESIGNALSTART, 0, 5));
			usleep(200 * 1000);
#endif
		}
	};
	thread t_listner(f_listner, &state, &serial, f);
	t_listner.detach();
}

void dch::Repeater::disconnect(bool* success) {

#ifdef _WIN32
	serial.write(DISCONNECT);
	Sleep(400);

	if (state != DISCONNECTED)
		return;
	
	serial.close();
	*success = false;
#else
	string msg = DISCONNECT;
	serial.writeMsg(msg.c_str(), msg.size());
	usleep(400 * 1000);

	if (!Helper::str_equals((char*)state.c_str(), DISCONNECTED, 0,5))
		return;

	serial.closePort();
	*success = false;
	
#endif
}


