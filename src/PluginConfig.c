#include "PluginConfig.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#ifdef _WIN32
#include <Windows.h>
#endif

FILE* fPtr;
char path[2048];
char f_data[DATASIZE];
unsigned int autoConnect = 0;

void getPath()
{				
	#ifdef _WIN32 
	char* username = getenv("username");
	snprintf(path, 1024, "%s%s%s", "C:/Users/", username, "/AppData/Roaming/TS3Client/plugins/tshamlink.conf"); 		
	#else
	char* username = getenv("USER");	
	snprintf(path, 2048, "%s%s%s", "/home/", username, "/.config/tshamlink.conf"); 			
	#endif	
}

void loadConfig()
{			
	getPath();	
	fPtr = fopen(path, "r+");
	if (NULL == fPtr)
	{
		fPtr = fopen(path, "w+");
		if (fPtr == NULL)
			return;

		getConfigStr(f_data);
		fputs(f_data, fPtr);					
	}
	else
	{
		fgets(f_data, sizeof(f_data), fPtr);		
		setConfigStr(f_data);
		fclose(fPtr);	
	}
}


void setConfigStr(char* data)
{	
	if (NULL != data && "" != data) {

		char* setting = strtok(data, ";");
		char* prop;
		char* key;
		char* value;

		unsigned int index;

		while (NULL != setting)
		{
			prop = setting;
			setting = strtok(NULL, ";");
			key = strtok(prop, ":");
			value = strtok(NULL, ":");
			if (!strcmp(key, "autoconnect"))
			{				
				autoConnect = strcmp(value, "0");
			}
		}
	}	
}

void getConfigStr(char* conf)
{		
	snprintf(conf, DATASIZE, "%s%d\n", "autoconnect:", autoConnect);	
}

void saveConfig()
{		 
	getPath();
	fPtr = fopen(path, "w+");
	if (NULL == fPtr)
		return;

	getConfigStr(f_data);	
	fputs(f_data, fPtr);
	fclose(fPtr);	
}
