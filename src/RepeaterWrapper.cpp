#include "Repeater.h"
#include "RepeaterWrapper.h"

namespace dch {
	extern "C" {

		Repeater* newRepeater()
		{		
			return new Repeater();
		}

		void Repeater_tryconnect(Repeater* r, boolean* success) {
			r->tryconnect((bool*)success);
				
		}
		void Repeater_disconnect(Repeater* r, boolean* success) {
			r->disconnect((bool*)success);
		}
								
		void Repeater_radioSend(Repeater* r) {
			r->radioSend();
		}
		void Repeater_radioStopSend(Repeater* r) {
			r->radioStopSend();
		}
		void Repeater_onResiving(void(*f)(boolean), Repeater* r) {
			r->onResiving((void(*)(bool))f);
		}
	}
}
