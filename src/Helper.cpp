#include "Helper.h"
#include <cstddef>
#include <string.h>

bool dch::Helper::str_equals(char* str_1, char* str_2, unsigned short s_scope, unsigned short e_scope)
{
	int l_str1 = strlen(str_1), l_str2 = strlen(str_2);
	for (size_t i = s_scope; i < e_scope; i++)
	{
		if ((l_str1 <= i || l_str2 <= i) || str_1[i] != str_2[i])
			return false;
	}
	return true;
}





