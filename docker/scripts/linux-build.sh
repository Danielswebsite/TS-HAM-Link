#!/bin/bash

# Declare a object to keep the args and thise values
declare -A args

# Clone project and fetch from git
git clone https://gitlab.com/Danielswebsite/TS-HAM-Link.git

# Go to git repo folder
cd TS-HAM-Link

# Create missing folders
mkdir bin && mkdir obj && mkdir logs

# Catch the args and parse them in to the object
for arg in $@; do
  if [[ $arg == "-"* ]]; then
    key=$arg
    args["$key"]=1 
  else
    args["$key"]="$arg"
  fi
done
unset key


# Func to check for is args exists
has_args() {
  if [[ "${args["-$1"]}" ]]; then 
    return 1
  else
    return 0
  fi
}


# Check for is 'branch' is part of args 
has_args branch
if [ $? -eq 1 ]; then
  BRANCH="${args["-branch"]}"
fi

# Checkout if not main branch
if [[ $BRANCH != "main" ]]; then
  git checkout $BRANCH
fi

# Move and build files
make -f ./Plugin-Linux.mk >> ./logs/build-linux-x64.log