GCC = g++
CFLAGS = -c -Wall

all: test clean

test: test.o Repeater.o RepeaterWrapper.o PluginConfig.o SerialPort.o Helper.o
	g++ -o bin/test.so obj/test.o obj/SerialPort.o obj/Helper.o obj/Repeater.o obj/RepeaterWrapper.o obj/PluginConfig.o

test.o:
	$(GCC) $(CFLAGS) -Isrc/test src/test/test.cpp -o obj/test.o

Repeater.o: ./src/Repeater.cpp
	$(GCC) $(CFLAGS) -Isrc src/Repeater.cpp -o obj/Repeater.o

RepeaterWrapper.o: ./src/RepeaterWrapper.cpp
	$(GCC) $(CFLAGS) -Isrc src/RepeaterWrapper.cpp -o obj/RepeaterWrapper.o

PluginConfig.o: ./src/PluginConfig.c
	$(GCC) $(CFLAGS) -Isrc src/PluginConfig.c -o obj/PluginConfig.o

SerialPort.o: ./src/SerialPort.cpp
	$(GCC) $(CFLAGS) -Isrc src/SerialPort.cpp -o obj/SerialPort.o

Helper.o: ./src/Helper.cpp
	$(GCC) $(CFLAGS) -Isrc src/Helper.cpp -o obj/Helper.o

clean:
	rm -rf obj/*.o	

