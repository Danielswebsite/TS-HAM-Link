#
# Makefile to build TS HAM Link Plugin for TeamSpeak 3 x64 for Windows
#

GCC = g++
CFLAGS = -c -O2 -Wall -fPIC -municode -std=c++17
PNAME = tshamlink_0_3_5

all: plugin clean

plugin: plugin.o Repeater.o RepeaterWrapper.o PluginConfig.o SerialPort.o Helper.o
	$(GCC) -o bin/$(PNAME).dll obj/plugin.o obj/SerialPort.o obj/Helper.o obj/Repeater.o obj/RepeaterWrapper.o obj/PluginConfig.o -shared -static

plugin.o: ./src/plugin.c
	$(GCC) $(CFLAGS) -D ADD_EXPORTS -Iinclude src/plugin.c -o obj/plugin.o

Repeater.o: ./src/Repeater.cpp
	$(GCC) $(CFLAGS) -D ADD_EXPORTS -Isrc src/Repeater.cpp -o obj/Repeater.o

RepeaterWrapper.o: ./src/RepeaterWrapper.cpp
	$(GCC) $(CFLAGS) -D ADD_EXPORTS -Isrc src/RepeaterWrapper.cpp -o obj/RepeaterWrapper.o

PluginConfig.o: ./src/PluginConfig.c
	$(GCC) $(CFLAGS) -D ADD_EXPORTS -Isrc src/PluginConfig.c -o obj/PluginConfig.o

SerialPort.o: ./src/SerialPort.cpp
	$(GCC) $(CFLAGS) -D ADD_EXPORTS -Isrc src/SerialPort.cpp -o obj/SerialPort.o

Helper.o: ./src/Helper.cpp
	$(GCC) $(CFLAGS) -D ADD_EXPORTS -Isrc src/Helper.cpp -o obj/Helper.o

clean:
	del .\obj\*.o
